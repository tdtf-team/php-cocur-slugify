Source: php-cocur-slugify
Section: php
Priority: optional
Maintainer: Teckids Debian Task Force <tdtf@lists.teckids.org>
Uploaders: Thorsten Glaser <tg@mirbsd.de>, Dominik George <natureshadow@debian.org>
Build-Depends: debhelper-compat (= 12), pkg-php-tools (>= 1.7~)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/cocur/slugify
Vcs-Git: https://salsa.debian.org/tdtf-team/php-cocur-slugify.git
Vcs-Browser: https://salsa.debian.org/tdtf-team/php-cocur-slugify

Package: php-cocur-slugify
Architecture: all
Depends: ${misc:Depends}, ${phpcomposer:Debian-require}
Recommends: ${phpcomposer:Debian-recommend}
Suggests: ${phpcomposer:Debian-suggest}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: Converts a string to a slug for PHP
 This library converts a string to a slug. It includes integrations for
 Symfony (2 and 3), Silex (1 and 2), Laravel, Twig (1 and 2),
 Zend Framework 2, Nette Framework, Latte and Plum.
 .
 Features:
  * Removes all special characters from a string.
  * Provides custom replacements for Arabic, Austrian, Azerbaijani,
    Brazilian Portuguese, Bulgarian, Burmese, Chinese, Croatian, Czech,
    Esperanto, Estonian, Finnish, French, Georgian, German, Greek,
    Hindi, Hungarian, Italian, Latvian, Lithuanian, Macedonian,
    Norwegian, Polish, Romanian, Russian, Serbian, Spanish, Swedish,
    Turkish, Ukrainian and Vietnamese special characters. Instead of
    removing these characters, Slugify approximates them (e.g., ae
    replaces ä).
  * No external dependencies.
  * PSR-4 compatible.
  * Compatible with PHP >= 5.5.9 and PHP 7.
